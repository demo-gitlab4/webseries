package selenium_program;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_page {
	
	@FindBy(xpath="//a[@id='welcome']") private WebElement Welcome;
	@FindBy(xpath="//a[text()='Logout']") private WebElement LogOut;
	
	public Home_page(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	public void homepage()
	{
		Welcome.click();
		LogOut.click();
	}
	

}
