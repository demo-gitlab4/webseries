package selenium_program;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Login_Class {
	
	@FindBy(xpath="//input[@id='txtUsername']") private WebElement UN;
	@FindBy(xpath="//input[@id='txtPassword']") private WebElement Pass;
	@FindBy(xpath="//input[@id='btnLogin']") private WebElement Login;
	
	
	public Login_Class(WebDriver driver)
	{
		PageFactory.initElements(driver,this);
	}
	
	public void Orange_hrm()
	{
		UN.sendKeys("Admin");
		Pass.sendKeys("admin123");
		Login.click();
	}


}
