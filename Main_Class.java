package selenium_program;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main_Class {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver","C:\\Users\\new\\Downloads\\webdriver\\chromedriver.exe");
		WebDriver driver =new ChromeDriver();
		driver.get("https://opensource-demo.orangehrmlive.com/");
		
		Login_Class Login=new Login_Class(driver);
		Login.Orange_hrm();
		
		Home_page logout=new Home_page(driver);
		logout.homepage();
		
		System.out.println("Done");
		
		
	}

}
